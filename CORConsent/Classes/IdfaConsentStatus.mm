/*
    This module retrieves IDFA tracking status in both the new and the old API. Returned values are compliant with new API from
    AppTrackingTransparency framework.
 */

#if defined(__IPHONE_14_0)
#import <AppTrackingTransparency/ATTrackingManager.h>
#endif
#import <AdSupport/AdSupport.h>

extern "C" {

    int GetAppleIdfaStatus(bool maskIfNotRequired) {
        
#if defined(__IPHONE_14_0)
        if(@available(iOS 14.0, macOS 11.0, tvOS 14.0, *)) {
            ATTrackingManagerAuthorizationStatus status = [ATTrackingManager trackingAuthorizationStatus];
           
            // While technically a cast to ulong/int could be returned, it gives us certianinty that apple won't change the underlying enum values
            switch (status) {
                case ATTrackingManagerAuthorizationStatusNotDetermined:
                    // In early iOS 14, if the IDFA is returned normaly even tho the status is NotDetermined, we can reurn Authorized.
                    // This is controlled with maskIfNotRequired parameter
                    if(maskIfNotRequired && [[[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString] isEqualToString:@"00000000-0000-0000-0000-000000000000"] == NO)
                    {
                        return 3;
                    }
                    
                    return 0;
                case ATTrackingManagerAuthorizationStatusRestricted: return 1;
                case ATTrackingManagerAuthorizationStatusDenied: return 2;
                case ATTrackingManagerAuthorizationStatusAuthorized: return 3;
                default: return 1;
            }
        }
        else
        {
#endif
            if ([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled] == YES)
                return 3;
            else
                return 2;
#if defined(__IPHONE_14_0)
        }
#endif
    }
}
